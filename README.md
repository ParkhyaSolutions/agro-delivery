# ABOUT THE PROJECT

At Agrostand , we’re transforming the Agri-value chain through technology. For farm owners, we enable digital access to a wider marketplace. For agribusinesses , we assure quality whilst mitigating counterparty risk and enabling secure payments. We also provide last mile pick from Agri-businesses and last mile pickup to Farmers with our Agro Delivery Facility.

# OBJECTIVES

Our platform provides access to agricultural inputs such as fertilizers, seeds, planting materials, and equipment at the touch of a button and farmers can create their own trade profiles and directly deal with agribusinesses .
On our app, farmers and agri-businesses can create their own trade terms and directly negotiate with agribusinesses . We also take care of transportation/Delivery (last mile pickup & drop) and payments , to drive higher efficiency in the Agri-value chain.
Farmers & Agri-Businesses benefit from the instant payments credited to their Bank account, as soon as the goods are delivered.
We will update weather data and provide forecasts, Provide Mandi rates and share real-time farming advice.
We leverage technology to help our buyers track goods at every step on the way and ensure on the time payment to farmers.
Our easy-to-use platform is designed to allow agribusinesses to directly negotiate with farmers and realise higher transparency in their supply chain.

# PRE-REQUEST

01. Have wi-fi or mobile data internet connection
02. Android device

# GOOGLE PLAY STORE

[Agro Delivery](https://play.google.com/store/apps/details?id=com.ps.ad&hl=en-GB)

# SCREENSHOTS

- **Select Language**

![alt text](images/Screenshot_from_2021-07-24_11-23-31.png "Select Language Screen")

- **Login Screen**

![alt text](images/Screenshot_from_2021-07-24_11-23-39.png "Login Screen")

# Development Technology
- App Development : **Android** 
- Backend         : **PHP Codeigniter** 

# DEVELOPER INFO

[Parkhya Solutions Pvt. Ltd.](https://www.parkhya.com/)
